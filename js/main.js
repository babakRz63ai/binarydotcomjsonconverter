function clearInput()
{
	document.getElementById("sourceJSON").value = null;
}

function clearOutput()
{
	document.getElementById("resultCSV").value = null;
}

function convert2CSV()
{
	var inputText = document.getElementById("sourceJSON").value;
	if (inputText==null) {
		showErrorMessage("There is no input", null);
		return;
	}
	
	if (inputText.length==0)
	{
		showErrorMessage("There is no input", null);
		return;
	}
	
	try {
		var myObj = JSON.parse(inputText);
		if (myObj.history===undefined)
		{
			showErrorMessage("The given JSON does not have neccessary field 'history'", null);
			return;
		}
		
		if (myObj.history===null)
		{
			showErrorMessage("history field of the given JSON is null", null);
			return;
		}
		
		if (myObj.history.prices===undefined)
		{
			showErrorMessage("history field of the given JSON does not have neccessary array 'prices'", null);
			return;
		}
		
		if (myObj.history.prices===null)
		{
			showErrorMessage("'prices' field of the given JSON is null", null);
			return;
		}
		
		if (myObj.history.times===undefined)
		{
			showErrorMessage("history field of the given JSON does not have neccessary array 'times'", null);
			return;
		}
		
		if (myObj.history.times===null)
		{
			showErrorMessage("'times' field of the given JSON is null", null);
			return;
		}
		
		result = "";
		for (i=0;i<myObj.history.times.length;i++)
			result += new Date(Number.parseInt(myObj.history.times[i])*1000)+", "+myObj.history.prices[i]+"\n";
		
		document.getElementById("resultCSV").value += result;
		
	}
	catch (error)
	{
		showErrorMessage("This is not a valid JSON", error.message);
		return;
	}
	
	document.getElementById("resultFrame").style.visibility="visible";
}

function showErrorMessage(message, moreDetails)
{
	if (moreDetails==null)
		alert(message);
	else
		alert(message+"\n("+moreDetails+")");
}
